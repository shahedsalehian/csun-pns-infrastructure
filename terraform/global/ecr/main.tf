terraform {
  backend "s3" {
    bucket = "saps-prod-state-bucket"
    dynamodb_table = "saps-prod-state-lock"
    key    = "ecr/ecr.tfstate"
    encrypt = true
    region = "us-west-2"
  }
}

provider "aws" {
  region  = "us-west-2"
}

module "ecr" {
  source = "../../modules/ecr"
  ecr-name = "csun-saps"
}