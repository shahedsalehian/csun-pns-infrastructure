terraform {
  backend "s3" {
    bucket = "saps-prod-state-bucket"
    dynamodb_table = "saps-prod-state-lock"
    key    = "s3/s3_prod_logs.tfstate"
    encrypt = true
    region = "us-west-2"
  }
}

provider "aws" {
  region  = "us-west-2"
}

module "s3" {
  source = "../../modules/s3"
  
  bucket_name = "saps-prod-logs-bucket"
}
