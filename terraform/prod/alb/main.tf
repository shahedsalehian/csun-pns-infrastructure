terraform {
  backend "s3" {
    bucket = "saps-prod-state-bucket"
    dynamodb_table = "saps-prod-state-lock"
    key    = "alb/alb.tfstate"
    encrypt = true
    region = "us-west-2"
  }
}

provider "aws" {
  region  = "us-west-2"
}

module "alb" {
    source = "../../modules/alb"
    name = "sapsBalancer"
    vpc_id = "${data.terraform_remote_state.vpc.vpc_id}"
    ssl_certificate_arn = "${data.aws_acm_certificate.domain.arn}"
    public_subnet_ids = [
                            "${data.terraform_remote_state.vpc.public_subnets[0]}",
                            "${data.terraform_remote_state.vpc.public_subnets[1]}"
                        ]
    health_check_path = "/"
    environment = "prod"
    access_log_bucket_name = "saps-prod-logs-bucket"
    # The port on which targets receive traffic
    # unless overridden when registering a specific target
    port = 80
    domain = "csun.xyz"
}