
data "terraform_remote_state" "vpc" {
  backend = "s3"

  config {
    bucket = "saps-prod-state-bucket"
    region = "us-west-2"
    key = "vpc/vpc.tfstate"
  }
}

data "aws_acm_certificate" "domain" {
  domain      = "*.csun.xyz"
  types       = ["AMAZON_ISSUED"]
  most_recent = true
}
