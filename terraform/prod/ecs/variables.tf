variable "project" {
  default = "saps-prod"  
}

variable "environment" {
  default = "prod"
}
