data "terraform_remote_state" "vpc" {
  backend = "s3"

  config {
    bucket = "saps-prod-state-bucket"
    region = "us-west-2"
    key = "vpc/vpc.tfstate"
  }
}

data "aws_acm_certificate" "domain" {
  domain      = "*.csun.xyz"
  types       = ["AMAZON_ISSUED"]
  most_recent = true
}

data "terraform_remote_state" "ecs_cluster" {
  backend = "s3"

  config {
    bucket = "saps-prod-state-bucket"
    region = "us-west-2"
    key = "ecs/ecs_cluster.tfstate"
  }
}

data "terraform_remote_state" "alb" {
  backend = "s3"

  config {
    bucket = "saps-prod-state-bucket"
    region = "us-west-2"
    key = "alb/alb.tfstate"
  }
}


