terraform {
  backend "s3" {
    bucket = "saps-prod-state-bucket"
    dynamodb_table = "saps-prod-state-lock"
    key    = "eip/eip.tfstate"
    encrypt = true
    region = "us-west-2"
  }
}

provider "aws" {
  region  = "us-west-2"
}

module "eip" {
  source = "../../modules/eip"
  nat-eip-name = "SAPS-PROD-NAT-EIP"
}
