terraform {
  backend "s3" {
    bucket = "saps-prod-state-bucket"
    dynamodb_table = "saps-prod-state-lock"
    key    = "vpc/vpc.tfstate"
    encrypt = true
    region = "us-west-2"
  }
}

provider "aws" {
  region  = "us-west-2"
}

module "vpc" {
  source = "../../modules/vpc"
  
  vpc_name = "saps-vpc-prod"
  vpc_env = "prod"
  remote_bucket_name = "saps-prod-state-bucket"
}