terraform {
  backend "s3" {
    bucket = "saps-staging-state-bucket"
    dynamodb_table = "saps-staging-state-lock"
    key    = "s3/s3_staging_logs.tfstate"
    encrypt = true
    region = "us-west-2"
  }
}

provider "aws" {
  region  = "us-west-2"
}

module "s3" {
  source = "../../modules/s3"

  bucket_name = "saps-staging-logs-bucket"
}
