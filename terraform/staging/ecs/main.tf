terraform {
  backend "s3" {
    bucket          = "saps-staging-state-bucket"
    dynamodb_table  = "saps-staging-state-lock"
    key             = "ecs/ecs.tfstate"
    encrypt = true
    region          = "us-west-2"
  }
}

provider "aws" {
  region  = "us-west-2"
}

resource "aws_ecs_task_definition" "app" {
  family                    = "application-stack"
  network_mode              = "awsvpc"
  requires_compatibilities  = ["FARGATE"]

  container_definitions     = "${file("task_definitions/csun-saps-staging.json")}"

  task_role_arn             = "${data.aws_iam_role.ecsTaskExecutionRole.arn}"
  execution_role_arn        = "${data.aws_iam_role.ecsTaskExecutionRole.arn}"

  cpu                       = 256
  memory                    = 512

  lifecycle {
    create_before_destroy   = true
  }
}

module "app-service" {
  source              = "../../modules/ecs"

  service_name        = "app"
  # container-name is for the loadbalancer to know which container to target
  container_name      = "frontend"
  environment         = "staging"

  private_subnet_ids  = "${data.terraform_remote_state.vpc.private_subnets}"
  target_group_id     = "${data.terraform_remote_state.alb.alb_target_group_id}"
  cluster_id          = "${data.terraform_remote_state.ecs_cluster.cluster_id}"

  vpc_id              = "${data.terraform_remote_state.vpc.vpc_id}"
  port                = 80

  desired_count       = 1


  cpu = 256
  memory = 512
}
