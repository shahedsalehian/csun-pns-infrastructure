terraform {
  backend "s3" {
    bucket = "saps-staging-state-bucket"
    dynamodb_table = "saps-staging-state-lock"
    key    = "eip/eip.tfstate"
    encrypt = true
    region = "us-west-2"
  }
}

provider "aws" {
  region  = "us-west-2"
}

module "eip" {
  source = "../../modules/eip"
  nat-eip-name = "SAPS-STAGING-NAT-EIP"
}
