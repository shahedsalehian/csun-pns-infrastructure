terraform {
  backend "s3" {
    bucket = "saps-staging-state-bucket"
    dynamodb_table = "saps-staging-state-lock"
    key    = "alb/alb.tfstate"
    encrypt = true
    region = "us-west-2"
  }
}

provider "aws" {
  region  = "us-west-2"
}

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config {
    bucket = "saps-staging-state-bucket"
    region = "us-west-2"
    key = "vpc/vpc.tfstate"
  }
}

data "aws_acm_certificate" "domain" {
  domain      = "*.csun.xyz"
  types       = ["AMAZON_ISSUED"]
  most_recent = true
}

module "alb" {
    source = "../../modules/alb"
    name = "sapsBalancer"
    vpc_id = "${data.terraform_remote_state.vpc.vpc_id}"
    ssl_certificate_arn = "${data.aws_acm_certificate.domain.arn}"
    public_subnet_ids = [
                            "${data.terraform_remote_state.vpc.public_subnets[0]}",
                            "${data.terraform_remote_state.vpc.public_subnets[1]}"
                        ]
    health_check_path = "/"
    environment = "staging"
    access_log_bucket_name = "saps-staging-logs-bucket"
    # The port on which targets receive traffic
    # unless overridden when registering a specific target
    port = 80
    domain = "csun.xyz"
}
