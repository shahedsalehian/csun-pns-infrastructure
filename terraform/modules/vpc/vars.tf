variable "remote_bucket_name" {
  description = "name of the remote eip bucket"
}

# variable "eip_remote_key" {
#   description = "eip key of the remote bucket"
# }

variable "vpc_name" {
  description = "name of the vpc including its environment"
}

variable "vpc_env" {
  description = "environment of the vpc for the tags"
}