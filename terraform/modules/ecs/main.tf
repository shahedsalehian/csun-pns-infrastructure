#####################
### LAUNCH CONFIG ###
#####################
resource "aws_launch_configuration" "ecs-launch-configuration" {
    name            = "ecs-launch-configuration"
    image_id        = "${data.aws_ami.amazon_linux_ecs.id}"
    iam_instance_profile = "${aws_iam_instance_profile.ecs-instance-profile.id}"
    instance_type   = "t2.micro"
    security_groups = ["${aws_security_group.allow_all.id}"]

    lifecycle {
        create_before_destroy = true
    }

    user_data = <<EOF
#!/bin/bash
echo ECS_CLUSTER=${var.cluster_name} >> /etc/ecs/ecs.config
EOF
}

######################
### SECURITY-GROUP ###
######################
resource "aws_security_group" "allow_all" {
    name = "allow_all"
    description = "Allow all inbound traffic"
    vpc_id = "${var.vpc_id}"
    
    ingress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }

}

#######################
### AUTOSCALING EC2 ###
#######################

resource "aws_autoscaling_group" "ec2" {
    name = "terraform-asg-ecs"
    launch_configuration = "${aws_launch_configuration.ecs-launch-configuration.name}"
    min_size = 1
    max_size = 2
    desired_capacity = 1
    vpc_zone_identifier = ["${var.private_subnet_ids}"]

    lifecycle {
        create_before_destroy = true
    }
}

resource "aws_autoscaling_policy" "ec2" {
  name                   = "autoscalingpolicy-terraform-test"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  policy_type            = "SimpleScaling"
  cooldown               = 300
  autoscaling_group_name = "${aws_autoscaling_group.ec2.name}"
}

resource "aws_cloudwatch_metric_alarm" "ec2" {
  alarm_name          = "terraform-cwm-ecs-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "75"

  dimensions = {
    AutoScalingGroupName = "${aws_autoscaling_group.ec2.name}"
  }

  alarm_description = "This metric monitors ec2 cpu utilization"
  alarm_actions     = ["${aws_autoscaling_policy.ec2.arn}"]
}

###################
### ECS SERVICE ###
###################

resource "aws_ecs_service" "main" {
  name                = "${var.name}"
  iam_role            = "${aws_iam_role.ecs-service-role.name}"
  cluster             = "${var.cluster_id}"
  task_definition     = "${aws_ecs_task_definition.app.id}"
  desired_count       = "${var.desired_count}"
  launch_type         = "EC2" 

  load_balancer {
    target_group_arn  = "${var.target_group_id}"
    container_name    = "${var.name}"
    container_port    = "${var.port}"
  }
}

resource "aws_ecs_task_definition" "app" {
  family                    = "application-stack"
  network_mode              = "bridge"
  requires_compatibilities  = ["EC2"]
  
  container_definitions     = "${file("${path.module}/task_definitions/csun-saps.json")}"
  
  lifecycle {
    create_before_destroy   = true
  }
}



#######################
### AUTOSCALING ECS ###
#######################
