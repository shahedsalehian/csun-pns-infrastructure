variable "environment" {}
variable "target_group_id" {}
variable "name" {}
variable "cluster_id" {}
variable "cluster_name" {}
variable "desired_count" {}
variable "private_subnet_ids" {
  type = "list"
}
variable "port" {}

variable "vpc_id" {}
