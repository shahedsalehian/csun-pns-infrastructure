variable "nat-eip-name" {
  description = "Name of the Elastic IP to be used by the NAT Gateway"
}