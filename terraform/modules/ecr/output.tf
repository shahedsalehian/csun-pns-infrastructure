output "url" {
  value = "${aws_ecr_repository.ecr.repository_url}"
}

output "registry_id" {
  value = "${aws_ecr_repository.ecr.registry_id}"
}