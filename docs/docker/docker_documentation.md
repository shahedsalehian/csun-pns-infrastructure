# SAPS - Docker documentation

## Table of Contents
- [Dockerfile](#dockerfile)
- [.dockerignore](#dockerignore)
- [docker-compose](#docker-compose)
- [Build and run application locally](#build-and-run-application-locally)
- [Docker commands](#docker-commands)

## Dockerfile

Docker uses a `Dockerfile` to build images.
This file in the repository is called `Dockerfile-dev` as it is only used with `docker-compose` for development purposes

```Dockerfile
FROM node:10-alpine

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install

# Bundle app source
COPY . .

EXPOSE 4200
CMD $(npm bin)/ng serve --host 0.0.0.0
```

This image is based off an alpine-based Node.js image.
We are installing angular and all the other dependencies for this project with `RUN npm install` and then exposing port `4200` to allow traffic to the Angular development server.

`CMD $(npm bin)/ng serve --host 0.0.0.0` This final command looks up the npm binaries for this project and then runs `ng serve` to start the Angular development server and binds the host to `0.0.0.0` to allow make sure that the traffic goes out of the container to the host machine.


## .dockerignore
The `.dockerignore` file prevent local modules and debug logs from being copied onto Docker image.

```
node_modules/*
npm-debug.log
data/*
```

# docker-compose
Docker compose allows us to run multi-container Docker applications.

 `version: '3.3'` refers to which compose file format that's being used. 3.3 uses the 17.06.0+ Docker Engine.

**docker-compose.yml**
```YML
version: '3.3'

services:
    backend:
        build:
            context: ./backend
        ports:
            - "3000:3000"
        depends_on:
            - mysql
        networks:
            - docker_network
        volumes:
            - ./backend:/usr/src/app
            - /usr/src/app/node_modules
        command: npm run dev
    mysql:
        build:
            context: ./mysql
            args:
                - MYSQL_DATABASE=default_database
                - MYSQL_USER=default_user
                - MYSQL_PASSWORD=secret
                - MYSQL_ROOT_PASSWORD=root
        volumes:
            - ./data/mysql/:/var/lib/mysql
        ports:
            - "3306:3306"
        networks:
            - docker_network
    frontend:
        build:
          context: ./frontend
          dockerfile: Dockerfile-dev
        ports:
            - "4200:4200"
        networks:
            - docker_network
        volumes:
            - ./frontend:/usr/src/app
            - /usr/src/app/node_modules
networks:
    docker_network:
        driver: bridge
```

## Example (Backend Container)
Let's take a look at the backend container:
```YML
    backend:
        build:
            context: ./backend
        ports:
            - "3000:3000"
        depends_on:
            - mysql
        networks:
            - docker_network
        volumes:
            - ./backend:/usr/src/app
            - /usr/src/app/node_modules
        command: npm run dev
```

### Build Context
The build context tells docker-compose where to look for the `Dockerfile` for the services that we are trying to build.

```YML
        build:
            context: ./backend
```

### Port Mapping
To access the container on our host machine, we need to map the container port to a host port (`Host:Container`).

```YML
        ports:
            - "3000:3000"
```

### Dependency
The `depends_on` directive will tell docker-compose to hold off on starting the container until the given container in the directive has STARTED; this does not mean that it is READY!

```YML
        depends_on:
            - mysql
```

### Networks

We need to define a container network, that our containers can join to communicate effectively in.

```YML
networks:
    docker_network:
        driver: bridge
```

A `bridge` driver creates a private network where only the containers within that network can communicate, outside traffic is only allowed through exposed ports.


We can tell each container which network to join, since a docker-compose file can have multiple networks. But we want all of our services to be in the same network.

```YML
        networks:
            - docker_network
```
`driver: bridge` creates a private internal network on the host so that containers are able to communicate with each other.

```
  networks:
      docker_network:
          driver: bridge
```
### Volumes

With volumes we can map a directory from our host machine to a directory on the container. In this project it is necessary to mount the application directory to reflect changes that are made locally inside the container, so that live changes can be made.

```YML
        volumes:
            - ./backend:/usr/src/app
            - /usr/src/app/node_modules
```

If only one directory is given in the `volumes` directive, it will map the container folder to the host machine. 

In this case, we are doing it to mount the `node_modules` that the container compiles.


## Build and Run application locally
1. Run `docker-compose build` to build the docker images
2. Run `docker-compose up` to START the containers
3. Run `docker-compose down` to STOP the containers
4. Run `curl -i localhost:3000` or visit http://localhost:3000/

This will start a Node.JS instance with Express.js and a MySQL DB which the Express.js server can connect to.

## Docker commands
- `docker ps` = List containers.
- `docker build` = Build image from Dockerfile
- `docker images` = List images.
- `docker ps -aq` = List all containers.
- `docker stop $(docker ps -aq)` = Stop all running containers.
- `docker rm $(docker ps -aq)` = Remove all containers.
- `docker rmi $(docker images -q)` = Remove all images.
