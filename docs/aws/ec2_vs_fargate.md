## EC2 On-Demand vs Fargate Potential Costs

-	EC2 On-Demand costs
    - t2.medium
    - t2.large
-	Fargate costs
    - Desired capacity
    - Maximum capacity

### EC2 On-Demand costs (t2.medium vs t2.large)
* Amazon uses 730 hours per month to calculate the price.
* Region: Oregon

#### t2.medium
vCPU: 2  
Memory: 4 GiB  
Price per Hour: $0.0464

Monthly cost = $0.0464 x 730 = $33.87  
(Monthly cost = Price per Hour x Hours of month)

#### t2.large
vCPU: 2  
Memory: 8 GiB  
Price per Hour: $0.0928  

Monthly cost = $0.0928 x 730 = $67.74  
(Monthly cost = Price per Hour x Hours of month)


### Fargate costs  

#### Desired capacity
Our service uses 4 ECS Tasks running at all times for a month (2,592,000 seconds) where each ECS Task uses 0.25 vCPU and 1GB memory.
- Production: 2
- Staging: 1
- Testing: 1

###### Monthly CPU charges:  
Total vCPU charges = # of Tasks x # vCPUs x price per CPU-second x CPU duration per day (seconds) x # of days.

- Number of Tasks: 4  
- Number of vCPUs: 0.25
- price per CPU-second: $0.00001406
- CPU duration per day (seconds): 86,400 seconds in a day
- Number of days: 30 days

Total vCPU charges = 4 x 0.25 x $0.00001406 x 86,400 x 30 = $36.44

###### Monthly memory charges:  
Total memory charges = # of Tasks x memory in GB x price per GB x memory duration per day (seconds) x # of days.

Total memory charges = 4 x 1 x 0.00000353 x 86,400 x 30 = $36.60

###### Monthly Fargate compute charges:
Monthly Fargate compute charges = monthly CPU charges + monthly memory charges

Monthly Fargate compute charges = $36.44 + $36.60 = $73.04

#### Maximum capacity
Total vCPU charges = 6 x 0.25 x $0.00001406 x 86,400 x 30 = $54.67
Total memory charges = 6 x 1 x 0.00000353 x 86,400 x 30 = $54.90

Monthly Fargate compute charges = $54.67 + $54.90 = $109.57

Our service uses 6 ECS Tasks running at all times for a month (2,592,000 seconds) where each ECS Task uses 0.25 vCPU and 1GB memory. Maximum capacity of tasks used to auto scale when needed.
- Production: 4
- Staging: 1
- Testing: 1
