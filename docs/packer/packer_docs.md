## PSN Documentation - Packer
Packer is a lightweight tool that is used to create pre-configured operating systems also called images. The following will go over how to create an images that has Ansible, Prometheus, and Node-exporter pre-configured.

- [Packer.json](#packer.json)
    - [Variables](#variables)
    - [Provisioner](#provisioner)
    - [Builders](#builders)
    - [Full Code](#full-code)
    - [Commands](#commands)
        - [Validate](#validate)
        - [Build](#build)

## Packer.json
Packer uses templates in the format of JSON in order to configure the image. Start by creating a new file called ``Packer.json``.

### Variables
Variables are needed so that we can pass our AWS credentials. 

    {   "variables": {
            "aws_access_key": "",
            "aws_secret_key": ""
        },

### Provisioner
Provisioners focus on what and where scripts should be executed. The first code block defines a shell script called ``provision.sh`` which installs Ansible. The second block references the Ansible script "test" which installs and configures Prometheus and Node-Exporter.

        "provisioners": [
            {
                "type": "shell",
                "script": "provision.sh"
            },
            {
                "type": "ansible-local",
                "playbook_dir": "../Ansible",
                "playbook_file": "../Ansible/playbooks/test.yml" 
            }
        ],

### Builders
Builders are responsible for the configuration of the image such as which AMI to use, which region, which VPC, which keypair etc. The following keypair ``test.pem`` has not been uploaded to the repository for safety reasons but it should be pretty simple to create a new key in the AWS console.

        "builders": [{
            "type": "amazon-ebs",
            "region": "us-west-2",
            "source_ami": "ami-0bbe6b35405ecebdb",
            "instance_type": "t2.micro",
            "vpc_id": "vpc-06bafdcb3d6eb3a86",
            "subnet_id": "subnet-038671578f17118d2",
            "ssh_keypair_name": "test",
            "ssh_private_key_file": "test.pem",
            "ssh_username": "ec2-user",
            "ami_name": "pns-ami {{timestamp}}"
        }]
    }

## Full Code
Below is the full code of ``Packer2.json``.

    {   "variables": {
            "aws_access_key": "",
            "aws_secret_key": ""
        },

        "provisioners": [
            {
                "type": "shell",
                "script": "provision.sh"
            },
            {
                "type": "ansible-local",
                "playbook_dir": "../Ansible",
                "playbook_file": "../Ansible/playbooks/test.yml" 
            }
        ],
        "builders": [{
            "type": "amazon-ebs",
            "region": "us-west-2",
            "source_ami": "ami-0bbe6b35405ecebdb",
            "instance_type": "t2.micro",
            "vpc_id": "vpc-06bafdcb3d6eb3a86",
            "subnet_id": "subnet-038671578f17118d2",
            "ssh_keypair_name": "test",
            "ssh_private_key_file": "test.pem",
            "ssh_username": "ec2-user",
            "ami_name": "pns-ami {{timestamp}}"
        }]
    }

## Commands
### Validate
packer validate <filename>

etc. ``packer validate packer2.json``

### Build
packer build <filename>

etc. ``packer build packer2.json``