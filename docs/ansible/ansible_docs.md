## PNS Documentation - Ansible

- [Install Ansible](#install-ansible)
- [Playbook test.yml](#playbook-test.yml)
- [Execute Playbook](#execute-playbook)

## Install Ansible
The file ``provision.sh`` is responsible for installing Ansible on a Linux machine. It will automatically be installed on the Packer image but it can also be installed manually by running the following command:

    sudo amazon-linux-extras install ansible2

## Playbook test.yml
The following playbook has been divided into steps and does the following:

**Prometheus**
- Update yum repo
- Create Prometheus user
- Download Prometheus
- Extract Prometheus
- Move Prometheus
- Move Prometheus Files
- Move Prometheus.yml
- Change ownership and Setup Service File
- Reload Prometheuse to register changes

**Node-exporter**
- Download node_exporter
- Extract node_exporter
- Move node_exporter
- Create user and service
- Move node_exporter.service
- Reload Node-exporter to register changes

    ---
    - hosts: all
    become: true
    tasks:
        - name: Update yum repo
            yum:
            name: '*'
            state: latest

        - name: Create Prometheus user
            shell: |
            useradd --no-create-home --shell /bin/false prometheus
            mkdir /etc/prometheus
            mkdir /var/lib/prometheus
            chown prometheus:prometheus /etc/prometheus
            chown prometheus:prometheus /var/lib/prometheus

        - name: Download Prometheus
            get_url:
            url: https://github.com/prometheus/prometheus/releases/download/v2.3.2/prometheus-2.3.2.linux-amd64.tar.gz
            dest: /home/ec2-user/prometheus-2.3.2.linux-amd64.tar.gz

        - name: Extract Prometheus
            unarchive:
            src: /home/ec2-user/prometheus-2.3.2.linux-amd64.tar.gz
            dest: /home/ec2-user/

        - name: Move Prometheus 
            shell: |
            mv /home/ec2-user/prometheus-2.3.2.linux-amd64 prometheus-files
            rm /home/ec2-user/prometheus-2.3.2.linux-amd64.tar.gz

        - name: Move Prometheus files
            shell: |
            cp prometheus-files/prometheus /usr/local/bin/
            cp prometheus-files/promtool /usr/local/bin/
            cp prometheus-files/promtool /usr/local/bin/
            chown prometheus:prometheus /usr/local/bin/prometheus
            chown prometheus:prometheus /usr/local/bin/promtool
            cp -r prometheus-files/consoles /etc/prometheus
            cp -r prometheus-files/console_libraries /etc/prometheus
            chown -R prometheus:prometheus /etc/prometheus/consoles
            chown -R prometheus:prometheus /etc/prometheus/console_libraries

        - name: Move Prometheus.yml
            shell: |
            mv /home/ec2-user/prometheus.yml /etc/prometheus/prometheus.yml

        - name: Change ownership and Setup Service File
            shell: |
            chown prometheus:prometheus /etc/prometheus/prometheus.yml
            mv /home/ec2-user/prometheus.service /etc/systemd/system/prometheus.service

        - name: Reload Prometheuse to register changes
            shell: |
            systemctl daemon-reload
            systemctl start prometheus
            systemctl status prometheus

        - name: Download node_exporter
            get_url:
            url: https://github.com/prometheus/node_exporter/releases/download/v0.16.0/node_exporter-0.16.0.linux-amd64.tar.gz
            dest: /home/ec2-user/node_exporter-0.16.0.linux-amd64.tar.gz

        - name: Extract node_exporter
            unarchive:
            src: /home/ec2-user/node_exporter-0.16.0.linux-amd64.tar.gz
            dest: /home/ec2-user/

        - name: Move node_exporter
            shell: |
            mv /home/ec2-user/node_exporter-0.16.0.linux-amd64/node_exporter /usr/local/bin/
            rm /home/ec2-user/node_exporter-0.16.0.linux-amd64.tar.gz

        - name: Create user and service
            shell: |
            useradd -rs /bin/false node_exporter

        - name: Move node_exporter.service
            shell: |
            mv /home/ec2-user/node_exporter.service /etc/systemd/system/node_exporter.service

        - name: Reload Node-exporter to register changes
            shell: |
            systemctl daemon-reload
            systemctl start node_exporter
            systemctl status node_exporter
            systemctl enable node_exporter


## Execute Playbook

    ansible-playbook test.yml

    ansible --version