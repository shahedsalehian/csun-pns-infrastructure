# AWS Certificate Manager

To ensure security of the data transmitted to our VPC, we need to enable TLS for all traffic routed to our domain.

Here's how we did it:

If a certificate file has been provided by someone else, it is important to upload the certificates manually to ACM.

However, if you own the domain and you have access to Route53, you can do DNS validation through Terraform.

```
resource "aws_acm_certificate" "cert" {
  domain_name       = "example.com"
  validation_method = "DNS"
}

data "aws_route53_zone" "zone" {
  name         = "example.com."
  private_zone = false
}

resource "aws_route53_record" "cert_validation" {
  name    = "${aws_acm_certificate.cert.domain_validation_options.0.resource_record_name}"
  type    = "${aws_acm_certificate.cert.domain_validation_options.0.resource_record_type}"
  zone_id = "${data.aws_route53_zone.zone.id}"
  records = ["${aws_acm_certificate.cert.domain_validation_options.0.resource_record_value}"]
  ttl     = 60
}
```

First, we are establishing a certificate, which is now pending validation. The validate the certificate via DNS validation, we have to add a CNAME record value to our hosted zone, where our domain resides.