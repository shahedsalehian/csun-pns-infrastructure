# Terragrunt

We use Terragrunt to automate the deployment of an entire environment.


# Installation

## MacOS - Homebrew

```brew install terragrunt```

## Linux - Linuxbrew

To install Linuxbrew - [https://github.com/Linuxbrew/brew](https://github.com/Linuxbrew/brew)

Use Linuxbrew to install terragrunt

```brew install terragrunt```

# Setting Up The Environment

Here's our production environment
```
├── prod
│   ├── alb
│   │   ├── data.tf
│   │   ├── main.tf
│   │   ├── output.tf
│   │   └── terraform.tfvars
│   ├── ecs
│   │   ├── data.tf
│   │   ├── main.tf
│   │   ├── output.tf
│   │   ├── terraform.tfvars
│   │   └── variables.tf
│   ├── ecs_cluster
│   │   ├── main.tf
│   │   ├── output.tf
│   │   └── terraform.tfvars
│   └── vpc
│       ├── main.tf
│       ├── output.tf
│       └── terraform.tfvars
...
```

Within every module directory place a `terraform.tfvars` file. 
This file is used by terragrunt to identify which modules to target.

This file can either be empty:

```
terragrunt {}
```

or you can define module dependencies

```
terragrunt = {
    dependencies {
        paths = ["../vpc", "../alb", "../ecs_cluster"]
    }
}
```

# Using Terragrunt

After the environment is setup and all the dependecies are declared, we can run terragrunt either from within the module or from the environment.

Since our goal is to run terragrunt for the entire environment, we are going to run the commands from the environment folder.
(`path/to/prod`) e.g. (`~/projects/csun-saps-infrastructure/terraform/prod`)

Once inside the environment folder you can use the following commands: 
```
terragrunt plan-all
terragrunt apply-all
terragrunt destroy-all
terragrunt output-all
terragrunt validate-all
```

Now you can replicate this in every other environment that you have. In our case: *prod* and *staging*