# Access Log Bucket
#### Notes
  * The access log bucket is just a "folder" in Amazon S3. Recall that an Amazon S3 bucket is a public cloud storage resource available in Amazon Web Services' (AWS) Simple Storage Service (S3). Amazon S3 buckets are similar to file folders, store objects which consist of data and their descriptive metadata.
  * The bucket must have a bucket  policy that grants Elastic Load Balancing permission to write the access logs to your bucket. The policy is written in JSON.
  * When you specify an S3 bucket for access logging, it must be located in the same region as the load balancer

We will enable access logging for the SAPS infrastructure. Conveniently, Amazon Elastic Load Balancing provides access logs that capture detailed information about requests sent to your load balancer. These logs contains information such as the time the request was received, the client's IP address, latencies, request paths, and server responses. You can use these access logs to analyze traffic patterns and troubleshoot issues and audit them for security purposes.

## Access Log Bucket Code
You can see in the below code snippet that the AWS S3 bucket is created as a resource and named "terraform_log_bucket". Versioning is enabled. The prevent destroy is used here to provide extra protection against the destruction of the bucket.

      data "aws_elb_service_account" "main" {}

      data "aws_caller_identity" "current" {}

      resource "aws_s3_bucket" "terraform_log_bucket" {
        bucket = "${var.bucket_name}"

        versioning {
          enabled = true
        }

        lifecycle {
          prevent_destroy = true
        }

## Policy
The policy ID along with other details are visible in the code below. The important portions are the actual permissions that this policy will grant. For example, we see "s3PutObject" under the line that starts: "Action" . This is to allow Elastic Load Balancing to store objects in the S3 bucket.
Ensure that "Allow" is entered after "Effect" to actually allow the policy to be applied. You must pay special attention to the format of the Amazon Resource Name (ARN) that you specify in the "Resource": line inside your JSON policy code.

        policy = <<POLICY
      {
        "Id": "Policy1546882088739",
        "Version": "2012-10-17",
        "Statement": [
          {
            "Sid": "Stmt1546882082718",
            "Action": [
              "s3:PutObject"
            ],
            "Effect": "Allow",
            "Resource": "arn:aws:s3:::${var.bucket_name}/ALB/AWSLogs/${data.aws_caller_identity.current.account_id}/*",
            "Principal": {
              "AWS": [
                "${data.aws_elb_service_account.main.arn}"
              ]
            }
          }
        ]
      }
      POLICY
      }
