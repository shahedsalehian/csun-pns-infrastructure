# Terraform Documentation

This will explain general concepts about Terraform and give you links to helpful guides, etc.

## How To Install Terraform

[Follow this Guide](https://www.terraform.io/intro/getting-started/install.html) to install Terraform.
Basically it will be a single binary that you just include in your Path and then you should be good to go!

Further in the getting started section, many other aspects of Terraform will be explained. 

### MAKE SURE TO BE VERY FAMILIAR WITH THE OFFICIAL DOCUMENTATION

[Documentation](https://www.terraform.io/docs/providers/aws/index.html)


## Tips

### Resource Sharing
It is essential to know that every file in a single folder is sharing data/resources. E.g. if you define the terraform backend/provider/data source in one file you don't have to redefine it in everyfile and you can use Data Sources in multiple files. The files are all aware of each other BUT ONLY IN THE SAME FOLDER. You cannot share resources across folders.


### State Locking
If multiple people are working on the same infrastructure at the same time, it is important to enable State Locking. For more info on that, see [here](https://www.terraform.io/docs/state/locking.html).

Basically state locking makes the state immutable while one person is working on it so that multiple people don't create state conflicts.

### Terraform Backend

Every folder needs one definition of a terraform backend to store the state of the infrastructure. That is so terraform can keep track of what is currently in our AWS account.

Example:

```bash
terraform {
  backend "s3" {
      bucket = "example-terraform-remote-state"
      region = "us-west-1"
      key    = "vpc/terraform.tfstate"
    }
  required_version = "> 0.11.0"
}
```
`key = "vpc/terraform.tfstate"` is the exact path to the terraform.tfstate file that will store all the states. To separate all the responsibilities, try to keep services modular by having different folders for them. These folders have to correspond to the folders in S3 to make storing the states more readable.


### Data Source / Remote State

```
data "terraform_remote_state" "ec2" {
  backend = "s3"

  config {
    bucket = "example-terraform-remote-state"
    region = "us-west-1"
    key    = "ec2/terraform.tfstate"
    name   = "ec2/terraform.tfstate"
  }
}
```

To get the remote state of another section of your infrastructure you can simply reference the .tfstate file and then you are able to refernence it by the name. For example: `${data.terraform_remote_state.ec2.*}`. Whatever name you give the resource that's what you have to reference it with. Then you can access any of the OUTPUT variables that the state has defined.

### Output Variables

For the state to be remotely accessible you have to explicitly define which values should be exposed. So in every folder you can simply put a `outputs.tf` file that has all those values defined and it can look something like this:

```bash
output "example" {
  value = "${aws_instance.app.id}"
}
```






