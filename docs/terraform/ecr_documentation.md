# SAPS ECR Documentation
Amazon ECR allows us to host and manage our docker images. Docker is used to push and pull our images to/from our registry.

* Create ECR
  * ecr.tf
  * var.tf
* Registry Authentication
  * output.tf


### Create ECR
**aws_ecr_repository** creates a new ECR resource with the name stored in **ecr-name** inside the var.tf file.

#### ecr.tf
```
resource "aws_ecr_repository" "ecr" {
  name = "${var.ecr-name}"
}
```

#### var.tf
```
variable "ecr-name" {
  description = "name of the ecr repo"
}
```

### Registry Authentication
**registry_id** allows us to retrieve a token that we use with docker login for authentication. This token is only valid for 12 hours.

**url** outputs the URL to our ECR.

#### output.tf

```
output "url" {
  value = "${aws_ecr_repository.ecr.repository_url}"
}

output "registry_id" {
  value = "${aws_ecr_repository.ecr.registry_id}"
}
```
