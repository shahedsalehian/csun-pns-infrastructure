## Glossary

#### Application Load Balancer
An ALB serves as the single point of contact for clients. The load balancer distributes incoming application traffic across multiple targets, such as EC2 instances, in multiple Availability Zones. An ALB operates at layer 7 of the OSI model.

#### Amazon DynamoDB
A fully managed NoSQL database service that provides fast and predictable performance with seamless scalability.

#### Amazon Resource Name (ARN)
A standardized way to refer to an AWS resource. For example: arn:aws:iam::123456789012:user/division_abc/subdivision_xyz/Bob.

#### Amazon Simple Storage Service (Amazon S3)
Storage for the internet. You can use it to store and retrieve any amount of data at any time, from anywhere on the web.

#### Bucket
Amazon Simple Storage Service (Amazon S3): A container for stored objects. Every object is contained in a bucket. For example, if the object named photos/puppy.jpg is stored in the johnsmith bucket, then authorized users can access the object with the URL http://johnsmith.s3.amazonaws.com/photos/puppy.jpg.

#### Elastic IP address
A fixed (static) IP address that you have allocated in Amazon EC2 or Amazon VPC and then attached to an instance. Elastic IP addresses are associated with your account, not a specific instance. They are elastic because you can easily allocate, attach, detach, and free them as your needs change. Unlike traditional static IP addresses, Elastic IP addresses allow you to mask instance or Availability Zone failures by rapidly remapping your public IP addresses to another instance.

#### Listener
A listener checks for connection requests from clients, using the protocol and port that you configure, and forwards requests to one or more target groups, based on the rules that you define. Each rule specifies a target group, condition, and priority. When the condition is met, the traffic is forwarded to the target group.

#### NAT
Network address translation. A strategy of mapping one or more IP addresses to another while data packets are in transit across a traffic routing device. This is commonly used to restrict internet communication to private instances while allowing outgoing traffic.

#### NAT gateway
A NAT device, managed by AWS, that performs network address translation in a private subnet, to secure inbound internet traffic. A NAT gateway uses both NAT and port address translation.

See Also NAT instance.

#### State file
The state file contains information about what real resources exist in the AWS infrastructure for each object defined in the terraform configuration files.

#### Subnet

A segment of the IP address range of a VPC that EC2 instances can be attached to. You can create subnets to group instances according to security and operational needs.

#### Region
A named set of AWS resources in the same geographical area. A Region comprises at least two Availability Zones.

#### Target group (ALB)
A target group is used to route requests to one or more registered targets. When you create each listener rule, you specify a target group and conditions. When a rule condition is met, traffic is forwarded to the corresponding target group. You can create different target groups for different types of requests.

#### Terraform Module Registry
Gives Terraform users easy access to templates for setting up and running their infrastructure with verified and community modules.

#### Versioning
Every object in Amazon S3 has a key and a version ID. Objects with the same key, but different version IDs can be stored in the same bucket. Versioning is enabled at the bucket layer using PUT Bucket versioning.

#### Virtual Private Cloud (VPC)
An elastic network populated by infrastructure, platform, and application services that share common security and interconnection.
