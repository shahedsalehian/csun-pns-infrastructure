## Design Document

### Title: CSUN SAPS - Infrastructure
### Date: 11/16/2018
### Authors: Alex E., Brian S., Erik B., Shahed S.
### Reviewers: Alex E., Brian S., Erik B., Shahed S.
### Approver(s): Leo G.
### Revision Number: 1

# Executive Summary

CSUN SAPS (Student Academic Progression System) is a system that allows students to request permission numbers for classes that are full or don't have all the necessary prerequesites for. The infrastructure behind it as of now is very brittle and there is no documentation. This projects main mission is to leave a solid foundation for the future of the application.

# Goals
- Replace current infrastructure with Docker containers
- Create a three stage environment
    - Production
    - Staging
    - Testing
- Implement a system metric tool like DataDog
- Enable AutoScaling to allow for reliability and high-availability
- Build a CI/CD Pipeline for the developers

# Non-Goals

- No development of the codebase
- Current environment stays untouched until environment can be replicated in a separate environment

# Background

The infrastructure of this application has been left abandoned by the prior group of people that worked on this project. The infrastructure went through multiple iterations and has finally settled on a single T2.Micro instance, which is non-redundant and not scalable. 

Terminology needed:
- Instance = Virtual Machine
- ECS = Elastic Container Service
- Task = Container
- Cluster = Logical grouping of tasks and services
- Service = Cluster of Tasks running together


# High-Level Design

A setup done with ECS, utilizing Docker Images, Clusters, Services and Tasks.

![CloudCraft Diagram](images/csun_saps_goal.png)


# Detailed Design
- Three services; one for each environment
    - Production
    - Staging
    - Testing
- Application Load Balancer is responsible for traffic routing between the services
- Static assets served by an S3 bucket behind a CDN
- CI/CD Pipeline setup with AWS CodeBuild, AWS CodeDeploy and AWS CodePipeline

# Alternatives Considered
![Leo's Diagram](images/leo_saps.png)

Pros:
- Very Accessible
- Easily Scalable
- Manual intervention possible with SSH

Cons:
- Can get expensive as more EC2 instances spin up during peak times
- A lot of overhead for single instances
  
# Security Concerns

- Being responsible with Level 1 Student Data
    - We are not being exposed to it, since sensitive data will be redacted for the test environment
- Credentials need to be accessed through AWS Secret Manager

